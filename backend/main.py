#!/usr/bin/env python3
import sys

from common.common import Common
from shops.cooperativa import Cooperativa
from shops.vea import Vea
from shops.walmart import Walmart

STORES = {'w': Walmart(), 'c': Cooperativa(), 'v': Vea()}

if __name__ == '__main__':
    Common.clean_output_file()
    if len(sys.argv) > 1:
        STORES[sys.argv[1]].process()
    else:
        for i in STORES.values():
            i.process()
