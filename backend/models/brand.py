from sqlalchemy import Column, Integer, String

from models.base import Base


class Brand(Base):
    __tablename__ = 'brand'
    id = Column('id', Integer, primary_key=True, unique=True)
    name = Column('name', String, nullable=False)

    def __init__(self, name):
        self.name = name.lower()
