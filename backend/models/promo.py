from models.base import Base
from sqlalchemy import Column, Integer, String, Float


class Promo(Base):
    __tablename__ = 'promo'
    id = Column('id', Integer, primary_key=True, unique=True)
    name = Column('name', String, nullable=False)
    quantity = Column('quantity', Integer, nullable=False)
    discount = Column('discount', Float, nullable=False)

    def __init__(self, name, quantity, discount):
        self.name = name.lower()
        self.quantity = quantity
        self.discount = discount
