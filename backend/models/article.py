from models.base import Base
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Float, Boolean
from sqlalchemy.orm import relationship, backref
from datetime import date


class Article(Base):
    __tablename__ = 'article'
    id = Column('id', Integer, primary_key=True, unique=False)
    brand_id = Column(Integer, ForeignKey('brand.id'))
    category_id = Column(Integer, ForeignKey('category.id'))
    shop_id = Column(Integer, ForeignKey('shop.id'))
    shop = relationship('Shop', backref=backref('shops', uselist=True))
    brand = relationship('Brand', backref=backref('brands', uselist=True))
    category = relationship('Category', backref=backref('categories', uselist=True))
    name = Column('name', String, nullable=False)
    last_update = Column('last_update', DateTime, nullable=False)
    image_url = Column('image_url', String)
    product_url = Column('product_url', String)
    stock = Column('stock', Integer)
    promo_id = Column(Integer, ForeignKey('promo.id'))
    promo = relationship('Promo', backref=backref('articles', uselist=True))
    original_price = Column('original_price', Float)
    unit_price = Column('unit_price', Float)
    promo_price = Column('promo_price', Float)
    is_pack = Column('is_pack', Boolean)

    def __init__(self, name, product_url, image_url, original_price, promo_price, unit_price, is_pack, brand,
                 category, promo, stock, shop):
        self.name = name.lower()
        self.product_url = product_url
        self.image_url = image_url
        self.last_update = date.today()
        self.original_price = original_price
        self.promo_price = promo_price
        self.unit_price = unit_price
        self.brand = brand
        self.shop = shop
        self.category = category
        self.promo = promo
        self.is_pack = is_pack
        self.stock = stock
