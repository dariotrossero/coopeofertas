from sqlalchemy import Column, Integer, String

from models.base import Base


class Shop(Base):
    __tablename__ = 'shop'
    id = Column('id', Integer, primary_key=True, unique=True)
    branch_office = Column('branch_office', String, nullable=False)
    name = Column('name', String, nullable=False)

    def __init__(self, name, branch_office):
        self.name = name.lower()
        self.branch_office = branch_office.lower()
