from sqlalchemy import Column, Integer, String

from models.base import Base


class Category(Base):
    __tablename__ = 'category'
    id = Column('id', Integer, primary_key=True, unique=True)
    name = Column('name', String, nullable=False)

    def __init__(self, name):
        self.name = name.lower()
