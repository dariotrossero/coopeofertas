import json
import math
import re
from datetime import date

import toolz
from common.common import Common
from common.logger import Logger
from common.telegram_bot import TelegramBot
from helpers.connection_helper import ConnectionHelper
from helpers.db_helper import DbHelper
from helpers.settings_helper import SettingsHelper
import urllib.parse

from models.article import Article
from models.category import Category
from models.shop import Shop
from models.base import session_factory
from models.promo import Promo


class Vea:
    STORE = 'VEA'
    _new_offers = False
    _ignore_saved = True
    _base_url = 'https://www.veadigital.com.ar'
    URL = _base_url + '/Comprar/HomeService.aspx/ObtenerArticulosPorDescripcionMarcaFamiliaLevex'
    list = {}
    _regex_mxn = r"(\d+)x(\d+)"
    _regex_pack = r"\d{1,2} [U|u]|\d{1,2} [U|u]nidades|\d{1,2}\-*\s*[P|p]ack|[P|p]ack\s*\d{1,2}|[S|s]ix [P|p]ack|[L|l]ata \d{1,2} [U|u]"
    connection_helper = ConnectionHelper()
    db_helper = DbHelper()

    def __init__(self):
        self._options = SettingsHelper.load_categories('settings/vea.yml')
        self._categories = self._options['categories']
        self._search_terms = self._options['search_for']
        self._sucursales = self._options['sucursales']
        token_file = self._options['bot_token_file']
        token = Common.read_file('settings/' + token_file)[0]
        self.bot = TelegramBot(token)
        self._enable_notifications = self._options['enable_notifications']
        self.session = session_factory()

    def setup(self, tienda_id):
        url = self._base_url + '/Geolocalizacion/Geolocalizacion.aspx/UpdateLocalSeleccionUsuario'
        data = {"idb": tienda_id}
        # 9122 capitan martinez
        # 9127 casanova

        response = self.connection_helper.post_json(url, payload=data, verify=False)
        if response.status_code != 200:
            raise Exception
        self.connection_helper.get(self._base_url + '/Login/PreHome.aspx')

    def get_products_by_text(self, text):
        data = '{{IdMenu:"",textoBusqueda:\"{0}\", producto:"", marca:"", pager:"", ordenamiento:0, precioDesde:"", precioHasta:""}}'.format(
            urllib.parse.quote(text))
        try:
            response = self.connection_helper.post(self.URL, data)
        except Exception as e:
            Logger.exception('Exception in ' + self.STORE)
            raise e

        return json.loads(json.loads(response.text)['d'])[
            'ResultadosBusquedaLevex']

    def get_products_by_id(self, search_id):
        search_id = str(search_id)
        data = '{{IdMenu:\"{0}\",textoBusqueda:"", producto:"", marca:"", pager:"", ordenamiento:0, precioDesde:"", precioHasta:""}}'.format(
            search_id)
        try:
            response = self.connection_helper.post(self.URL, data)
        except Exception as e:
            Logger.exception('Exception in ' + self.STORE)
            raise e

        return json.loads(json.loads(response.text)['d'])[
            'ResultadosBusquedaLevex']

    def get_offers_list(self, search_term, sucursal, is_id, category):
        elements = []
        search_term = str(search_term)
        if is_id:
            for el in self.get_products_by_id(search_term):
                offer_list = self.filter_offers(el, sucursal, category)
                if len(offer_list) > 0:
                    elements.append(offer_list)
        else:
            for el in self.get_products_by_text(search_term):
                offer_list = self.filter_offers(el, sucursal, category)
                if len(offer_list) > 0:
                    elements.append(offer_list)
        return sorted(elements, key=lambda i: i['marca'])

    def filter_offers(self, product, sucursal, categoria):
        product_info = {}
        if len(product['Descuentos']) > 0:
            product_info['id'] = product['IdArticulo']
            description = product['DescripcionArticulo']
            product_info['descripcion'] = description
            product_info['pack'] = False
            product_info['categoria'] = categoria

            product_info['link'] = self.generate_articule_link(product['IdArticulo'], product[
                'DescripcionArticulo'])
            price = float(product['Precio'])
            product_info['Precio'] = price
            product_info['sucursal'] = sucursal
            product_info['imagen'] = self.generate_image_link(product['IdArchivoBig'])
            product_info['marca'] = product['Grupo_Marca'].capitalize()
            product_info['stock'] = product['Stock']
            product_info['Descuentos_raw'] = product['Descuentos']
            matches_pack = re.findall(self._regex_pack,
                                      product_info['descripcion'])
            if len(matches_pack) == 1:
                product_info['pack'] = True
        return product_info

    def get_offers(self, search_term, is_id):
        try:
            if is_id:
                suffix = self._categories[search_term]
            else:
                suffix = search_term
            filename = '{0}_{1}.json'.format(self.STORE, Common.normalize_string(
                str(suffix)))
            json_response = []
            for i in self._sucursales:
                self.setup(i)
                json_response = json_response + self.get_offers_list(search_term, self._sucursales[i], is_id, suffix)
            grouped_by_description = toolz.itertoolz.groupby('descripcion', json_response)
            json_response_without_duplicates = []
            for key in grouped_by_description:
                if len(grouped_by_description[key]) > 1:
                    grouped_by_description[key][0]['stock'] = {
                        grouped_by_description[key][0]['sucursal']: grouped_by_description[key][0]['stock'],
                        grouped_by_description[key][1]['sucursal']: grouped_by_description[key][1]['stock']}
                    grouped_by_description[key][0]['sucursal'] = [grouped_by_description[key][0]['sucursal'],
                                                                  grouped_by_description[key][1]['sucursal']]
                else:
                    grouped_by_description[key][0]['stock'] = {
                        grouped_by_description[key][0]['sucursal']: grouped_by_description[key][0]['stock']}
                    grouped_by_description[key][0]['sucursal'] = [grouped_by_description[key][0]['sucursal']]
                json_response_without_duplicates.append(grouped_by_description[key][0])
            json_response_without_duplicates = sorted(json_response_without_duplicates, key=lambda i: i['marca'])
            loaded_json = Common.load_json(filename)
            if self._ignore_saved or loaded_json != json_response:
                Common.save_json(json_response_without_duplicates,
                                 'offers/{0}_{1}.json'.format(
                                     self.STORE, Common.normalize_string(
                                         str(suffix))))
                Common.print_output(json_response)
                self.save_into_db(json_response_without_duplicates)
                if len(json_response) > 0:
                    Logger.info('Hay nuevas ofertas para {0}'.format(
                        suffix))
                    self._new_offers = True
                    if self._enable_notifications:
                        pass  # self.bot.send_message(self.parse_json(json_response))
                else:
                    Logger.info('No hay nuevas ofertas para {0}'.format(
                        suffix))
                    self._new_offers = self._new_offers or False
            else:
                Logger.info('No hay nuevas ofertas para {0}'.format(
                    suffix))
                self._new_offers = self._new_offers or False
        except Exception:
            Logger.exception('Exception in ' + self.STORE)

    def parse_json(self, json_input):
        r = ''
        string = '{0}\n{1}\n{2}: {3}.\n\n'
        for i in json_input:
            r = r + string.format(
                i['Descuento'].encode('ascii', 'ignore').decode('ascii'),
                i['descripcion'].encode('ascii', 'ignore').decode('ascii'),
                'Total $' + i['Total'].encode('ascii', 'ignore').decode('ascii'),
                'c/u $' + i['Individual'].encode('ascii', 'ignore').decode('ascii'))
        return r

    def parse_pack_string(self, string):
        if 'six' in string.lower():
            return 6
        else:
            return int(re.findall(r"\d{1,2}", string)[0])

    def process(self):
        Logger.info('Procesando {0}...'.format(self.STORE))
        for key in self._categories.keys():
            self.get_offers(key, True)
        for key in self._search_terms:
            self.get_offers(key, False)
        if not self._new_offers and self._enable_notifications:
            self.bot.send_message('No hay nuevas ofertas')

    def save_into_db(self, offers):
        session = self.session
        for offer in offers:
            promo = self.get_promo(offer)
            raw_promo = promo[1]
            promo = promo[0]
            marca = self.db_helper.get_marca(self.session, offer)
            categoria = self.db_helper.get_categoria(self.session, offer)

            precio_anterior = raw_promo['original_price']
            precio_promo = raw_promo['promo_price']
            is_pack = offer['pack']

            if is_pack:
                precio_unitario = Common.round_value(
                    float(precio_promo) / self.parse_pack_string(offer['descripcion']))
            else:
                precio_unitario = precio_promo

            for branch_office in offer['stock']:
                shop = self.db_helper.get_shop(self.session, 'Vea', branch_office)
                article = session.query(Article).filter_by(name=offer['descripcion'].lower(), shop_id=shop.id)
                if article.first() is None:

                    session.add(Article(offer['descripcion'], offer['link'], offer['imagen']
                                        , precio_anterior, precio_promo,
                                        precio_unitario, is_pack, marca, categoria, promo, offer['stock'][branch_office],
                                        shop))
                else:
                    article = article.first()
                    article.original_price = precio_anterior
                    article.promo_price = precio_promo
                    article.promo = promo
                    article.last_update = date.today()
                    article.is_pack = is_pack
                    article.unit_price = precio_unitario
            session.commit()

    def get_promo(self, product):
        session = self.session
        raw_data = self.get_offer_type(product)
        promo_quantity = raw_data['cantidad_promo']
        discount = raw_data['descuento']
        promo_name = raw_data['nombre_promo']
        promo = session.query(Promo).filter_by(quantity=raw_data['cantidad_promo'],
                                               discount=raw_data['descuento']).first()
        if promo is None:
            promo = Promo(promo_name, promo_quantity, discount)
            session.add(promo)
        return promo, raw_data

    def get_offer_type(self, product_info):
        is_pack = False
        promo = {"nombre_promo": None,
                 "cantidad_promo": None, "descuento": None, 'original_price': None,
                 'promo_price': None}
        matches_pack = re.findall(self._regex_pack,
                                  product_info['descripcion'])
        if len(matches_pack) == 1:
            is_pack = True

        discounts = product_info['Descuentos_raw'][0]
        if discounts['Tipo'] == 'Porcentaje':
            discount = int(discounts['Subtipo'])
            quantity = 1
            original_price = float(product_info['Precio'])
            promo_price = Common.round_value(original_price - original_price * (discount * 0.01))
            promo['nombre_promo'] = "{0}% de descuento".format(discount)
            promo['cantidad_promo'] = quantity
            promo['descuento'] = discount
            promo['original_price'] = original_price
            promo['promo_price'] = promo_price
        elif discounts['Tipo'] == '2do con rebaja':
            discount = int(discounts['Subtipo']) / 2
            original_price = float(product_info['Precio'])
            promo_price = Common.round_value(original_price - original_price * (discount * 0.01))
            quantity = 2
            promo['nombre_promo'] = "llevando 2, {0}% de descuento".format(discount)
            promo['cantidad_promo'] = quantity
            promo['descuento'] = discount
            promo['original_price'] = original_price
            promo['promo_price'] = promo_price
        elif discounts['Tipo'] == 'Precio fijo':
            quantity = 1
            price = float(discounts['Subtipo'])
            original_price = float(product_info['Precio'])
            discount = math.floor(abs(Common.round_value((price * 100 / original_price) - 100)))
            promo_price = Common.round_value(original_price - original_price * (discount * 0.01))
            promo['cantidad_promo'] = quantity
            promo['descuento'] = discount
            promo['original_price'] = original_price
            promo['promo_price'] = promo_price
            promo['nombre_promo'] = "{0}% de descuento".format(discount)

        elif discounts['Tipo'] == 'MxN':
            values = re.match(self._regex_mxn,
                              discounts['Subtipo'])
            quantity = int(values.group(1))
            m = int(values.group(2))
            discount = math.floor(Common.round_value(100 - 100 * m / quantity))
            original_price = float(product_info['Precio'])
            promo_price = Common.round_value(original_price - original_price * (discount * 0.01))
            if quantity > 1:
                nombre_promo = "llevando {0}, {1}% de descuento".format(quantity, discount)
            else:
                nombre_promo = "{0}% de descuento".format(discount)
            promo['nombre_promo'] = nombre_promo
            promo['cantidad_promo'] = quantity
            promo['descuento'] = discount
            promo['original_price'] = original_price
            promo['promo_price'] = promo_price
        else:
            pass  # log
        if is_pack:
            promo['precio_unitario_pack'] = Common.round_value(
                promo['promo_price'] / self.parse_pack_string(matches_pack[0]))
        return promo

    def generate_articule_link(self, article_id, descripcion):
        return 'https://www.veadigital.com.ar/prod/' + article_id + '/' + descripcion.replace(' ', '-')

    def generate_image_link(self, image_id):
        return "https://www.veadigital.com.ar/VeaComprasArchivos/Archivos/{0}".format(
            image_id)
