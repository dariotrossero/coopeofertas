import re
from datetime import date

from common.common import Common
from common.logger import Logger
from common.telegram_bot import TelegramBot
from helpers.connection_helper import ConnectionHelper
from helpers.db_helper import DbHelper
from helpers.settings_helper import SettingsHelper
from models.article import Article
from models.base import session_factory
from models.promo import Promo
import math


class Walmart:
    STORE = 'WALMART'

    _regex_plus = r".*(\d+)[x|X](\d+).*"
    _regex_percent = r".*[l|L]lev[á|a]s (\d) (\d+)"
    _regex_take_N_fixed_price = r".*[l|L]lev[á|a]s (\d), \$(\d+)"
    _regex_take_N_fixed_price_v2 = r".*[l|L]lev[á|a]s (\d)\s*[x|X]\s*\$(\d+)"
    _regex_second_at = r".*2d[a|o] al (\d+)"
    _regex_pack = r".*\d{1,2}\s*[P|p]ack|[p|P]ack\s*\d\d{1,2}|[p|P]ack\s*[x|X]*\s*\d{1,2}|\d+\s*[U|u]n"
    _new_offers = False
    _ignore_saved = True
    connection_helper = ConnectionHelper()
    db_helper = DbHelper()
    _URL = 'https://www.walmart.com.ar/api/catalog_system/pub/products/search/'
    _SEARCH_PARAMS = {'sl': '63c6cac5-a4b5-4191-a52a-65582db8f8b3',
                      'sc': 19
                      }
    list = {}

    def __init__(self):
        self._options = SettingsHelper.load_categories('settings/walmart.yml')
        self._categories = self._options['categories']
        token_file = self._options['bot_token_file']
        token = Common.read_file('settings/' + token_file)[0]
        self.bot = TelegramBot(token)
        self._enable_notifications = self._options['enable_notifications']
        self.session = session_factory()

    # noinspection PyMethodMayBeStatic
    def normalize(self, string):
        return float(string.replace('$', '').replace('.', '').replace(',', '.').strip())

    def get_offers(self, search_id):
        offers = []
        filename = '{0}_{1}.json'.format(self.STORE, self._categories[str(search_id)])
        json_response = self.get_offers_list(search_id)
        Common.print_output(json_response)
        if self._ignore_saved or Common.load_json(filename) != json_response:

            if len(json_response) > 0:
                Logger.info(self.STORE + ':Hay nuevas ofertas para {0}'.format(
                    self._categories[search_id]))
                for p in json_response:
                    product = self.get_product_details(p, self._categories[str(search_id)])
                    if product is not None:
                        offers.append(product)
                Common.save_json(offers, 'offers/{0}_{1}.json'.format(
                    self.STORE, self._categories[str(search_id)]))
                self.save_into_db(offers)
                self._new_offers = True
                if self._enable_notifications:
                    self.bot.send_message(self.parse_json(offers))
            else:
                Logger.info(self.STORE + ':No hay nuevas ofertas para {0}'.format(
                    self._categories[search_id]))
                self._new_offers = self._new_offers or False
        else:
            Logger.info('No hay nuevas ofertas para {0}'.format(
                self._categories[search_id]))
            self._new_offers = self._new_offers or False

    def has_offer(self, item):
        seller_info = item['items'][0]['sellers'][0]['commertialOffer']
        teasers = seller_info['Teasers']
        list_price = seller_info['ListPrice']
        price = seller_info['Price']
        if price <= list_price - Common.percentage(20, list_price) or len(teasers) > 0:
            return True
        return False

    def get_product_details(self, product, category):
        product_info = {}
        seller_info = product['items'][0]['sellers'][0]['commertialOffer']
        teasers = seller_info['Teasers']
        list_price = seller_info['ListPrice']
        price = seller_info['Price']
        product_info['pack'] = False
        product_info['categoria'] = category
        product_info['id'] = product['productId']
        product_info['imagen'] = product['items'][0]['images'][0]['imageUrl']
        product_info['descripcion'] = product['productName']
        product_info['price'] = price
        product_info['list_price'] = list_price
        product_info['precio_unitario'] = price
        product_info['link'] = product['link']
        product_info['marca'] = product['brand'].capitalize()
        product_info['stock'] = seller_info['AvailableQuantity']
        matches_pack = re.findall(self._regex_pack,
                                  product_info['descripcion'])
        if seller_info['AvailableQuantity'] == 0:
            return
        if len(teasers) > 0:
            product_info['promo'] = teasers[0]['<Name>k__BackingField']
            matches_n_x_m = re.match(self._regex_plus, product_info['promo'])
            matches_percent = re.match(self._regex_percent, product_info['promo'])
            matches_take_n_fixed_price = re.match(self._regex_take_N_fixed_price, product_info['promo'])
            matches_second_at = re.match(self._regex_second_at, product_info['promo'])

            if matches_take_n_fixed_price:
                product_info['cantidad_promo'] = int(matches_take_n_fixed_price.group(1).encode('ascii',
                                                                                                'ignore').decode(
                    'ascii'))
                product_info['price'] = float(matches_take_n_fixed_price.group(2).encode('ascii',
                                                                                         'ignore').decode(
                    'ascii'))
                total_without_discount = product_info['cantidad_promo'] * list_price
                total_with_discount = product_info['price']

                product_info['descuento_porcentaje_promo'] = 100 - (total_with_discount * 100 / total_without_discount)
            if matches_n_x_m:
                n = int(matches_n_x_m.group(1).encode('ascii',
                                                      'ignore').decode(
                    'ascii'))
                m = int(matches_n_x_m.group(2).encode('ascii',
                                                      'ignore').decode(
                    'ascii'))
                product_info['cantidad_promo'] = n
                product_info['descuento_porcentaje_promo'] = 100 - 100 * m / n
                product_info['tipo_descuento'] = 1
                unit_price = str(Common.round_value(
                    price * m / n))

            elif matches_second_at:
                product_info['cantidad_promo'] = 2
                product_info['descuento_porcentaje_promo'] = int(str(matches_second_at.group(1))) / 2
                product_info['tipo_descuento'] = 3
                unit_price = str(Common.round_value(
                    price -
                    price *
                    float(float(matches_second_at.group(1)) / 2 / 100)))
            elif matches_percent:
                product_info['cantidad_promo'] = int(matches_percent.group(1))
                product_info['descuento_porcentaje_promo'] = int(matches_percent.group(2))
                product_info['tipo_descuento'] = 4
                offer_string = \
                    'Llevando {0}, pagas {1}% menos'.format(
                        matches_percent.group(1),
                        matches_percent.group(2))
                unit_price = str(
                    Common.round_value(
                        self.normalize(price) - (
                                self.normalize(
                                    price) / 100 *
                                float(
                                    matches_percent.group(
                                        2)))))
                product_info['promo'] = offer_string
                product_info['precio_unitario'] = unit_price
            if len(matches_pack) == 1:
                product_info['pack'] = True
                product_info['precio_unitario_pack'] = float(
                    Common.round_value(float(product_info['precio_unitario']) / int(re.findall(r"\d{1,2}", matches_pack[0])[0])))
        else:
            if len(matches_pack) == 1:
                product_info['pack'] = True
                product_info['precio_unitario_pack'] = float(
                    Common.round_value(price / int(re.findall(r"\d{1,2}", matches_pack[0])[0])))
            product_info['tipo_descuento'] = 2
            product_info['cantidad_promo'] = 1
            product_info['promo'] = 'Ahorras {0}%'.format(Common.round_value(100 - (price * 100 / list_price)))
            product_info['descuento_porcentaje_promo'] = int(Common.round_value(100 - (price * 100 / list_price)))

        return product_info

    def get_offers_list(self, search_id):
        products_list = []
        items = []
        lower_limit = 0
        upper_limit = 49
        self._SEARCH_PARAMS['_from'] = lower_limit
        self._SEARCH_PARAMS['_to'] = upper_limit
        self._SEARCH_PARAMS['fq'] = search_id
        response = self.connection_helper.get(self._URL, params=self._SEARCH_PARAMS)
        if response.status_code not in [200, 206]:
            Logger.error('Api call: {0}, Status code {1}...'.format(self._URL, response.status_code))
            return []
        elements = response.json()
        products_list = products_list + elements
        while len(elements) != 0:  # no more elements, stop looping.
            lower_limit = lower_limit + 50
            upper_limit = upper_limit + 50
            self._SEARCH_PARAMS['_from'] = lower_limit
            self._SEARCH_PARAMS['_to'] = upper_limit
            response = self.connection_helper.get(self._URL, params=self._SEARCH_PARAMS)
            if response.status_code not in [200, 206]:
                Logger.error('Api call: {0}, Status code {1}...'.format(self._URL, response.status_code))
                break
            elements = response.json()
            products_list = products_list + elements

        for item in products_list:
            if self.has_offer(item):
                items.append(item)
        return sorted(items, key=lambda i: i['brand'])

    def parse_json(self, json):
        r = ''
        string = '{0}\n{1}\n{2}: {3}.\n\n'
        for i in json:
            r = r + string.format(i['descripcion'], '$' + str(i['price']), i['promo'],
                                  '$' + str(i['precio_unitario']) + ' c/u')
        return r

    def process(self):
        Logger.info('Procesando {0}...'.format(self.STORE))
        for key in self._categories.keys():
            self.get_offers(key)
        if not self._new_offers and self._enable_notifications:
            self.bot.send_message('No hay nuevas ofertas')

    def save_into_db(self, offers):
        session = self.session
        for offer in offers:
            marca = self.db_helper.get_marca(self.session, offer)
            promo = self.get_promo(offer)
            categoria = self.db_helper.get_categoria(self.session, offer)
            matches_pack = re.findall(self._regex_pack,
                                      offer['descripcion'].lower())

            precio_anterior = float(offer['list_price'])
            precio_promo = Common.round_value(precio_anterior - precio_anterior * (promo.discount / 100))
            shop = self.db_helper.get_shop(self.session, 'Walmart', 'Cabrera')
            article = session.query(Article).filter_by(name=offer['descripcion'].lower(), shop_id=shop.id)
            if len(matches_pack) == 1:
                is_pack = True
            else:
                is_pack = False
            if is_pack:
                precio_unitario = offer['precio_unitario_pack']
            else:
                precio_unitario = precio_promo

            if article.first() is None:
                session.add(Article(offer['descripcion'], offer['link'], offer['imagen']
                                    , precio_anterior, precio_promo,
                                    precio_unitario, is_pack, marca, categoria, promo, offer['stock'],
                                    shop))
            else:
                article = article.first()
                article.original_price = precio_anterior
                article.promo_price = precio_promo
                article.promo = promo
                article.last_update = date.today()
                article.is_pack = is_pack
                article.unit_price = precio_unitario
        session.commit()

    def get_promo(self, product):
        session = self.session
        promo = self.get_offer_type(product)
        cantidad_promo = promo['cantidad_promo']
        descuento = promo['descuento']
        promo_name = promo['nombre_promo']
        promo = session.query(Promo).filter_by(quantity=promo['cantidad_promo'], discount=promo['descuento']).first()
        if promo is None:
            promo = Promo(promo_name, cantidad_promo, descuento)
            session.add(promo)
        return promo

    def parse_pack_string(self, string):
        if 'six' in string.lower():
            return 6
        else:
            return int(re.findall(r"\d{1,2}", string)[0])

    def get_offer_type(self, product_info):
        matches_n_x_m = re.match(self._regex_plus, product_info['promo'])
        matches_percent = re.match(self._regex_percent, product_info['promo'])
        matches_take_n_fixed_price = re.match(self._regex_take_N_fixed_price, product_info['promo'])
        matches_take_n_fixed_price_v2 = re.match(self._regex_take_N_fixed_price_v2, product_info['promo'])
        matches_second_at = re.match(self._regex_second_at, product_info['promo'])
        if matches_take_n_fixed_price:
            quantity = int(matches_take_n_fixed_price.group(1).encode('ascii', 'ignore').decode('ascii'))
            price = float(matches_take_n_fixed_price.group(2).encode('ascii', 'ignore').decode('ascii')) / 2
            discount = math.floor(abs(Common.round_value((price * 100 / product_info['price']) - 100)))
            if quantity > 1:
                nombre_promo = "llevando {0}, {1}% de descuento".format(quantity, discount)
            else:
                nombre_promo = "{0}% de descuento".format(discount)
            return {"nombre_promo": nombre_promo,
                    "cantidad_promo": quantity, "descuento": discount}
        if matches_take_n_fixed_price_v2:
            quantity = int(matches_take_n_fixed_price_v2.group(1).encode('ascii', 'ignore').decode('ascii'))
            price = float(matches_take_n_fixed_price_v2.group(2).encode('ascii', 'ignore').decode('ascii')) / 2
            discount = math.floor(abs(Common.round_value((price * 100 / product_info['price']) - 100)))
            if quantity > 1:
                nombre_promo = "llevando {0}, {1}% de descuento".format(quantity, discount)
            else:
                nombre_promo = "{0}% de descuento".format(discount)
            return {"nombre_promo": nombre_promo,
                    "cantidad_promo": quantity, "descuento": discount}
        if matches_n_x_m:
            quantity = int(matches_n_x_m.group(1).encode('ascii', 'ignore').decode('ascii'))
            m = int(matches_n_x_m.group(2).encode('ascii', 'ignore').decode('ascii'))
            discount = math.floor(Common.round_value(100 - 100 * m / quantity))
            if quantity > 1:
                nombre_promo = "llevando {0}, {1}% de descuento".format(quantity, discount)
            else:
                nombre_promo = "{0}% de descuento".format(discount)
            return {"nombre_promo": nombre_promo,
                    "cantidad_promo": quantity, "descuento": discount}

        if matches_second_at:
            quantity = 2
            discount = math.floor(int(str(matches_second_at.group(1))) / 2)
            return {"nombre_promo": "llevando {0}, {1}% de descuento".format(quantity, discount),
                    "cantidad_promo": quantity, "descuento": discount}
        if matches_percent:
            quantity = int(matches_percent.group(1))
            discount = math.floor(int(matches_percent.group(2)))
            if quantity > 1:
                nombre_promo = "llevando {0}, {1}% de descuento".format(quantity, discount)
            else:
                nombre_promo = "{0}% de descuento".format(discount)
            return {"nombre_promo": nombre_promo,
                    "cantidad_promo": quantity, "descuento": discount}
        else:
            price = product_info['price']
            list_price = product_info['list_price']
            discount = math.floor(Common.round_value(100 - (price * 100 / list_price)))
            return {"nombre_promo": "{0}% de descuento".format(discount), "cantidad_promo": 1,
                    "descuento": discount}
