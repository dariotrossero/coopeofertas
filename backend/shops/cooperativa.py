import json
import re
from datetime import date
from common.common import Common
from common.logger import Logger
from common.telegram_bot import TelegramBot
from helpers import db_helper
from helpers.connection_helper import ConnectionHelper
from helpers.db_helper import DbHelper
from helpers.settings_helper import SettingsHelper
from models.article import Article
from models.base import session_factory
from models.brand import Brand
from models.category import Category
from models.promo import Promo
from models.shop import Shop


class Cooperativa:
    STORE = 'COOPERATIVA'
    # must be moved to settings file

    cookies = {}
    _new_offers = False
    _ignore_saved = True
    connection_helper = ConnectionHelper()
    db_helper = DbHelper()
    _regex_pack = r"\d{1,2}[U|u]ni|[X|x]\d{1}"

    _URL = "https://www.lacoopeencasa.coop/ws/index.php/categoria/" \
           "categoriaController/categorias_filtrado"
    _ONLY_PROMO = 'true'
    _CUSTOM_SEARCH_URL = "https://www.lacoopeencasa.coop/ws/index.php/categoria/categoriaController/filtros_busqueda"

    def __init__(self):
        self._options = SettingsHelper.load_categories('settings/cooperativa.yml')
        self._categories = self._options['categories']
        self._search_terms = self._options['search_for']
        token_file = self._options['bot_token_file']
        token = Common.read_file('settings/' + token_file)[0]
        self.bot = TelegramBot(token)
        self._enable_notifications = self._options['enable_notifications']
        self.session = session_factory()

    def pre_process_payload(self, json_response):
        products = json_response['datos']['articulos']
        for p in products:
            del (p['orden'])

    def initialize(self):
        self.connection_helper.post('https://www.lacoopeencasa.coop/ws/index.php/comun/'
                                    'autentificacionController/autentificar_invitado', payload='')

    def get_product_details(self, product, product_list):
        if self.has_promo(product):
            if product['precio_promo'] is not None:
                info = '{0} {1}. Llevando: {2}, ${3} c/u\n\n' \
                    .encode('ascii', 'ignore') \
                    .decode('ascii').format(
                    product['marca'],
                    product['descripcion'],
                    product['cantidad_promo'],
                    product['precio_promo']
                )
                product_list.append(info)
            else:
                info = '{0} {1}. Llevando: {2}, ${3} c/u \n**COMBO**\n\n' \
                    .encode('ascii', 'ignore').decode('ascii') \
                    .format(
                    product['marca'],
                    product['descripcion'],
                    product['cantidad_promo'],
                    product['precio_anterior']
                )
                product_list.append(info)

    def get_products(self, json):
        products = json['articulos']
        return sorted(products, key=lambda i: i['marca_desc'])

    def remove_non_offers(self, products_array):
        res = []
        for x in products_array:
            if self.has_promo(x):
                x['link'] = 'https://www.lacoopeencasa.coop/producto/' + x['descripcion'].replace(' ',
                                                                                                  '') + '/' + \
                            x['cod_interno']
                res.append(x)
        return res

    def has_promo(self, prod):
        return prod['cantidad_promo'] is not None or \
               prod['descuento_porcentaje_promo'] is not None or \
               prod['cantidad_promo'] is not None

    def get_offers(self, search_term, is_id):
        self.initialize()
        product_list = []
        if is_id:
            payload = {
                "id_busqueda": search_term,
                "pagina": 0,
                "filtros": {
                    "preciomenor": -1,
                    "preciomayor": -1,
                    "marca": [],
                    "categoria": [],
                    "tipo_seleccion": "categoria",
                    "filtros_gramaje": [],
                    "cant_articulos": 0,
                    "ofertas": self._ONLY_PROMO,
                    "modificado": "false",
                    "primer_filtro": ""
                }
            }
            search_term = self._categories[int(search_term)]
            filename = 'offers/{0}_{1}.json'.format(self.STORE, Common.normalize_string(search_term))
            url = self._URL
        else:
            filename = 'offers/{0}_{1}.json'.format(self.STORE,
                                                    Common.normalize_string(search_term))
            url = self._CUSTOM_SEARCH_URL
            payload = {
                "pagina": 0,
                "filtros": {
                    "preciomenor": -1,
                    "preciomayor": -1,
                    "categoria": [],
                    "marca": [],
                    "tipo_seleccion": "busqueda",
                    "tipo_relacion": "busqueda",
                    "filtros_gramaje": [],
                    "termino": Common.normalize_string(search_term),
                    "cant_articulos": 0,
                    "ofertas": 'false',
                    "modificado": "false",
                    "primer_filtro": ""
                }
            }
        response = None
        try:
            response = self.connection_helper.post_json(url, payload=payload)
        except Exception as e:
            Logger.exception('Exception in ' + self.STORE)

        if response.status_code != 200:
            Logger.error(
                'Error en la respuesta. Status code: {0}\nResponse: {1}\nPost to: {2}'.format(
                    response.status_code, response.content, self._URL))
            self.bot.send_message('Error en la respuesta al consultar por {0}'
                                  .format(search_term))
            raise Exception('Error en la consulta')

        json_response = json.loads(response.text)
        loaded_json = Common.load_json(filename)
        self.pre_process_payload(json_response)
        if loaded_json != json_response or self._ignore_saved:
            # estado=1 ok
            # estado=3 bad
            products = self.get_products(json_response['datos'])
            only_offers = self.remove_non_offers(products)
            only_offers = self.normalize_json(only_offers, search_term)
            self.save_into_db(only_offers)
            Common.save_json(only_offers, filename)
            Common.print_output(json_response)

            if len(products) > 0:
                for p in products:
                    self.get_product_details(p, product_list)
                s = ''.join(product_list)
                Logger.info('Hay nuevas ofertas para {0}'.format(
                    search_term))
                self._new_offers = True
                if self._enable_notifications:
                    self.bot.send_message(s)
            else:
                Logger.info('No hay nuevas ofertas para {0}'.format(
                    search_term))
                self._new_offers = self._new_offers or False

        else:
            Logger.info('No hay nuevas ofertas para {0}'.format(
                search_term))
            self._new_offers = self._new_offers or False

    def process(self):
        Logger.info('Procesando {0}...'.format(self.STORE))
        for key in self._categories.keys():
            self.get_offers(key, True)
        for key in self._search_terms:
            self.get_offers(key, False)
        if not self._new_offers and self._enable_notifications:
            self.bot.send_message('No hay nuevas ofertas')

    def normalize_json(self, json_obj, category):
        for x in json_obj:
            x['marca'] = x.pop('marca_desc').capitalize()
            x['descripcion'] = Common.capitalize_every_word(x['descripcion'])
            x['categoria'] = Common.capitalize_every_word(category)
        return json_obj

    def save_into_db(self, only_offers):
        session = self.session
        for offer in only_offers:
            promo = self.get_promo(offer)
            marca = self.db_helper.get_marca(self.session, offer)
            categoria = self.db_helper.get_categoria(self.session, offer)
            matches_pack = re.findall(self._regex_pack,
                                      offer['descripcion'].lower())

            precio_anterior = float(offer['precio_anterior'])
            if (offer['precio_promo']) is None:
                precio_promo = precio_anterior
            else:
                precio_promo = float(offer['precio_promo'])
            shop = self.db_helper.get_shop(self.session, 'Cooperativa', 'Shopping')
            article = session.query(Article).filter_by(name=offer['descripcion'].lower(), shop_id=shop.id)
            units = 1
            if len(matches_pack) == 1:
                is_pack = True
                units = self.parse_pack_string(matches_pack[0])
            else:
                is_pack = False
            if is_pack:
                precio_unitario = Common.round_value(precio_promo / units)
            else:
                precio_unitario = precio_promo

            if article.first() is None:
                session.add(Article(offer['descripcion'], offer['link'], offer['imagen']
                                    , precio_anterior, precio_promo,
                                    precio_unitario, is_pack, marca, categoria, promo, 999, shop))
            else:
                article = article.first()
                article.original_price = precio_anterior
                article.promo_price = precio_promo
                article.promo = promo
                article.last_update = date.today()
                article.is_pack = is_pack
                article.unit_price = precio_unitario
        session.commit()

    def get_promo(self, product):
        session = self.session
        cantidad_promo = int(product['cantidad_promo'])
        precio_anterior = float(product['precio_anterior'])
        if product['precio_promo'] is None:
            precio_promo = precio_anterior
        else:
            precio_promo = float(product['precio_promo'])
        if product['descuento_porcentaje_promo'] is None:
            descuento_porcentaje_promo = Common.round_value(100 - (precio_promo * 100 / precio_anterior))
        else:
            descuento_porcentaje_promo = float(product['descuento_porcentaje_promo'])
        if descuento_porcentaje_promo is None:
            promo_name = "llevando {0}".format(cantidad_promo)
        elif cantidad_promo == 1:
            promo_name = "{0}% de descuento".format(descuento_porcentaje_promo)
        elif descuento_porcentaje_promo == 0:
            promo_name = "llevando {0}".format(cantidad_promo)
        else:
            promo_name = "llevando {0}, {1}% de descuento".format(cantidad_promo, descuento_porcentaje_promo)

        promo = session.query(Promo).filter_by(quantity=cantidad_promo, discount=descuento_porcentaje_promo).first()
        if promo is None:
            promo = Promo(promo_name, cantidad_promo, descuento_porcentaje_promo)
            session.add(promo)
        return promo

    def parse_pack_string(self, string):
        if 'six' in string.lower():
            return 6
        else:
            return int(re.findall(r"\d{1,2}", string)[0])
