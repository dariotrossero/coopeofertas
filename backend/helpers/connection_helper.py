import requests


class ConnectionHelper(object):

    def __init__(self):
        self._headers = {
            'content-type': "application/json",
            'cache-control': "no-cache"
        }
        self.session = requests.session()

    def get(self, url, params=None):
        if params is None:
            params = {}
        response = self.session.get(url, headers=self._headers, params=params)
        return response

    def post(self, url, payload):
        response = self.session.post(url, data=payload, headers=self._headers)
        return response

    def post_json(self, url, payload, verify=True):
        response = self.session.post(url, json=payload, headers=self._headers, verify=verify)
        return response
