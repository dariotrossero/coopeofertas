from models.base import session_factory
from models.brand import Brand
from models.category import Category
from models.shop import Shop


class DbHelper:

    def __init__(self):
        pass

    def get_shop(self, session, shop_name, branch_office):
        shop = session.query(Shop).filter_by(name=shop_name.lower(), branch_office=branch_office.lower()).first()
        if shop is None:
            shop = Shop(shop_name.lower(), branch_office)
            session.add(shop)
        return shop

    def get_marca(self, session, product):
        marca = session.query(Brand).filter_by(name=product['marca'].lower()).first()
        if marca is None:
            marca = Brand(product['marca'])
            session.add(marca)
        return marca

    def get_categoria(self, session, product):
        categoria = session.query(Category).filter_by(name=product['categoria'].lower()).first()
        if categoria is None:
            categoria = Category(product['categoria'])
            session.add(categoria)
        return categoria
