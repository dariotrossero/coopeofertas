import yaml


class SettingsHelper:
    def __init__(self):
        pass

    @staticmethod
    def get_setting(setting):
        with open('settings.yml') as file:
            settings = yaml.load(file, Loader=yaml.FullLoader)
        return settings[setting]

    @staticmethod
    def load_categories(path):
        with open(path) as file:
            documents = yaml.load(file, Loader=yaml.FullLoader)
        return documents
