import logging


class Logger:
    logging.basicConfig(filename='app.log', filemode='a',
                        format='%(asctime)s - %(message)s',
                        level=logging.ERROR)

    @staticmethod
    def info(*args):
        logging.info(args)

    @staticmethod
    def error(*args):
        logging.error(args)

    @staticmethod
    def warning(*args):
        logging.warning(args)

    @staticmethod
    def exception(*args):
        logging.exception(args)
