import time

import telegram

from common.logger import Logger


class TelegramBot:

    def __init__(self, token):
        self.bot = telegram.Bot(token=token)

    def send_message(self, msg):
        try:
            chat_id = 154645571
            self._send_message(self.bot, chat_id=chat_id, text=msg)
        except Exception as e:
            Logger.error(e.message)

    def _send_message(self, bot, chat_id, text, **kwargs):
        if len(text) <= telegram.constants.MAX_MESSAGE_LENGTH:
            return bot.send_message(chat_id, text=text, **kwargs)

        parts = []
        while len(text) > 0:
            if len(text) > telegram.constants.MAX_MESSAGE_LENGTH:
                part = text[:telegram.constants.MAX_MESSAGE_LENGTH]
                first_lnbr = part.rfind('\n')
                if first_lnbr != -1:
                    parts.append(part[:first_lnbr])
                    text = text[first_lnbr:]
                else:
                    parts.append(part)
                    text = text[telegram.constants.MAX_MESSAGE_LENGTH:]
            else:
                parts.append(text)
                break

        msg = None
        for part in parts:
            msg = bot.send_message(chat_id, part, **kwargs)
            time.sleep(1)
        return msg  # return only the last message
