import json
import os
import re
from decimal import Decimal

from common.logger import Logger
from helpers.settings_helper import SettingsHelper


class Common:

    def __init__(self):
        pass

    @staticmethod
    def save_json(data, name):
        full_path = os.path.dirname(os.path.dirname(__file__)) + '/' + name
        path = os.path.dirname(full_path)
        if not os.path.exists(path):
            os.makedirs(path)
        with open(name, 'w+') as outfile:
            json.dump(data, outfile)
            outfile.close()

    @staticmethod
    def load_json(name):
        try:
            with open(name) as f:
                return json.load(f)
        except IOError:
            Logger.warning('Filename {0} not found'.format(name))
            return None

    @staticmethod
    def rename(filename_old, filename):
        os.rename(filename, filename_old)

    @staticmethod
    def normalize_string(filename):
        return re.sub('[^a-zA-Z0-9\.]', '_', filename)

    @staticmethod
    def round_value(value):
        dec = Decimal(value)
        return round(dec, 2)

    @staticmethod
    def capitalize_every_word(string):
        return " ".join([
            word.capitalize()
            for word in string.split(" ")])

    @staticmethod
    def percentage(percent, whole):
        return (percent * whole) / 100.0

    @staticmethod
    def print_output(text):
        if SettingsHelper.get_setting('text_output') == 'console':
            print(text)
        else:
            redirect_to_file(text)

    @staticmethod
    def clean_output_file():
        redirect_to_file('', False)

    @staticmethod
    def read_file(filename):
        f = open(filename, 'r')
        return f.readlines()


def redirect_to_file(text, amend=True):
    write_type = 'a+' if amend else 'w'
    f = open('output.txt', write_type)
    f.write(json.dumps(text) + '\n')
    f.close()
