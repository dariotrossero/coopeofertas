URLS:
params:
order_by=[discount|promo_price|unit_price]
sort=[asc|desc]

API endpoints:
get all the products from a given shop and category: /api/v1/<shop>/<category>
e.g:
/api/v1/walmart/jugos
get all the products from a given category: /api/v1/category/<category>
e.g:
/api/v1/category/vinos%20tintos
get all the products from a given shop: /api/v1/shop/<shop>
e.g:
/api/v1/shop/cooperativa