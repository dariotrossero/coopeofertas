# -*- coding: utf-8 -*-
# https://fastapi.tiangolo.com/tutorial/sql-databases/

import sys, os
sys.path.append(os.path.abspath(os.path.join('..', 'backend'))) 
from flask import Flask
from flask import request, jsonify

import os
import re

import datetime
from flask import render_template, redirect

from models.base import session_factory
from models.brand import Brand
from models.article import Article
from models.shop import Shop
from models.promo import Promo
from models.category import Category

session = session_factory()

_columns = 3
_ip_filter = r"(192|127.0.0.1|172|10)"
app = Flask(__name__)
app.config.from_pyfile("application.cfg", silent=True)
app.config['JSON_AS_ASCII'] = False

_SHOPS = ["COOPERATIVA", "VEA", "WALMART"]


@app.context_processor
def utility_processor():
    def format_category(c):
        if c.lower() == "paniales":
            c = "pañales"
        return c.capitalize()

    def get_all_categories():
        result = []
        for i in session.query(Category.name).all():
            result.append(i.name)
        return sorted(result)

    def get_categories(shop):
        result = []
        for i in session.query(Category).join(Article).join(Shop).filter(Shop.name == shop.lower()).all():
            result.append(i.name)
        return sorted(result)

    def get_shops():
        return _SHOPS

    return dict(
        get_categories=get_categories,
        get_shops=get_shops,
        get_all_categories=get_all_categories,
        format_category=format_category,
    )


@app.route("/")
def home():
    return render_template("index.html")

@app.route("/ofertas")
def ofertas():
    return render_template("base.html")

@app.route("/sickchill")
def visit_sickchill():
    return redirect("http://192.168.0.2:8081")

@app.route("/deluge_app")
def visit_deluge():
    return redirect("http://192.168.0.2:8112")

@app.route("/minidlna")
def visit_minidlna():
    return redirect("http://192.168.0.2:8200")

@app.route("/plex")
def visit_plex():
    return redirect("http://192.168.0.2:32400")

@app.route("/<shop>/<category>")
def offer(shop, category):
    shop = shop.lower()
    category = category.lower()
    data = get_articles_by_shop_and_category(shop, category, request.args.get('order_by'), request.args.get('sort'))
    products = [
        data[x: x + _columns] for x in range(0, len(data), _columns)
    ]
    return render_template("shop.html", elements={'items':products, 'shop':shop, 'category':category})


@app.route("/productos/<category>")
def offers_by_category(category):
    data = get_articles_by_category(category, request.args.get('order_by'), request.args.get('sort'))
    products = [
        data[x: x + _columns] for x in range(0, len(data), _columns)
    ]
    return render_template('products.html',elements={'items': products, 'category': category})


@app.route("/<shop>")
def offers_by_shop(shop):
    if "favicon.ico" == shop.lower():
        return ""
    else:
        if not re.match(_ip_filter, request.remote_addr):
            save_ip(request.remote_addr, shop.capitalize())
        shop = shop.lower()
        data = get_articles_by_shop(shop, request.args.get('order_by'), request.args.get('sort'))
        products = [
            data[x: x + _columns] for x in range(0, len(data), _columns)
        ]
        return render_template("shop.html", elements=products)


@app.route("/errors")
def errors():
    return render_template("errors.html", elements=see_errors())


@app.route("/stats/ips")
def ips():
    ip_list = see_ips()
    return render_template("stats_ips.html", elements=ip_list)


@app.route('/api/v1/shop/<shop>', methods=['GET'])
def api_offers_by_shop(shop):
    shop = shop.lower()
    data = get_articles_by_shop(shop, request.args.get('order_by'), request.args.get('sort'))
    r = []
    for element in data:
        r.append({
            "article_name": element.Article.name,
            "brand_name": element.Article.brand.name,
            "category_name": element.Article.category.name,
            "promo_name": element.Article.promo.name,
            "article_original_price": element.Article.original_price,
            "article_promo_price": element.Article.promo_price})
    return jsonify(r)


@app.route('/api/v1/category/<category>', methods=['GET'])
def api_offers_by_category(category):
    category = category.lower()
    data = get_articles_by_category(category, request.args.get('order_by'), request.args.get('sort'))
    r = []
    for element in data:
        r.append({
            "article_name": element.Article.name,
            "brand_name": element.Article.brand.name,
            "shop_name": element.Article.shop.name,
            "promo_name": element.Article.promo.name,
            "article_original_price": element.Article.original_price,
            "article_promo_price": element.Article.promo_price,
            "stock": element.Article.stock,
            "branch_office": element.Article.shop.branch_office})
    return jsonify(r)


@app.route('/api/v1/<shop>/<category>', methods=['GET'])
def api_offers_by_shop_and_category(shop, category):
    shop = shop.lower()
    category = category.lower()
    data = get_articles_by_shop_and_category(shop, category, request.args.get('order_by'), request.args.get('sort'))
    r = []
    for element in data:
        r.append({
            "article_name": element.Article.name,
            "brand_name": element.Article.brand.name,
            "promo_name": element.Article.promo.name,
            "article_original_price": element.Article.original_price,
            "article_promo_price": element.Article.promo_price,
            "stock": element.Article.stock,
            "branch_office": element.Article.shop.branch_office})
    return jsonify(r)


def see_ips():
    matrix = []
    exists = os.path.exists("ips.txt")
    if exists:
        with open("ips.txt", "r") as f:
            lines = f.readlines()
            for line in lines[::-1]:
                a = line.split(",")
                matrix.append({"date": a[0], "address": a[1], "resource": a[2]})
        return matrix
    else:
        return []


def see_errors():
    regex = r"(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3})"
    filename = "../back/app.log"
    exists = os.path.exists(filename)
    words = []
    content = None
    if exists:
        with open(filename, "r") as file:
            content = file.read()
    split = re.split(regex, content)
    for i in split:
        words.append(i.split('\n'))
    del (words[0])
    p = []
    for x in range(len(words)):
        if x % 2 == 0:
            p.append({'date': words[x], 'stacktrace': words[x + 1]})

    return reversed(p)


def get_articles_by_shop(shop, order_by=None, sort=None):
    query = session.query(Article, Shop).join(Article.brand).filter(Article.promo_id == Promo.id).filter(
        Article.shop_id == Shop.id).filter(Shop.name == shop.lower()).filter(
        Article.last_update > datetime.date.today())
    return get_filter(order_by, query, sort)


def get_articles_by_category(category, order_by=None, sort=None):
    query = session.query(Article, Category).join(Article.brand).filter(Article.promo_id == Promo.id).filter(
        Article.category_id == Category.id).filter(Category.name == category.lower()).filter(
        Article.last_update > datetime.date.today())
    return get_filter(order_by, query, sort)


def get_filter(order_by, query, sort):
    if order_by == 'promo_price':
        if sort == 'desc':
            return query.order_by(Article.promo_price.desc()).all()
        else:
            return query.order_by(Article.promo_price.asc()).all()
    elif order_by == 'unit_price':
        if sort == 'desc':
            return query.order_by(Article.unit_price.desc()).all()
        else:
            return query.order_by(Article.unit_price.asc()).all()
    elif order_by == 'discount':
        if sort == 'desc':
            return query.order_by(Promo.discount.desc()).all()
        else:
            return query.order_by(Promo.discount.asc()).all()
    else:
        if sort == 'desc':
            return query.order_by(Brand.name.desc()).all()
        else:
            return query.order_by(Brand.name.asc()).all()


def get_articles_by_shop_and_category(shop, category, order_by=None, sort=None):
    query = session.query(Article, Shop, Category).join(Article.brand).join(Article.promo).filter(
        Article.shop_id == Shop.id).filter(Article.promo_id == Promo.id).filter(
        Article.category_id == Category.id).filter(Category.name == category.lower()).filter(
        Shop.name == shop.lower()).filter(Article.last_update > datetime.date.today())
    return get_filter(order_by, query, sort)


def save_ip(ip, resource):
    if not os.path.exists("ips.txt"):
        os.mknod("ips.txt")
    f = open("ips.txt", "a+")
    f.write(
        datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S")
        + ","
        + ip
        + ","
        + resource
        + "\n"
    )
    f.close()


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
